'use strict';
//Popup
function POPUP_FEATURE() {
  var _wrap = "<div class='layout-popup-wrap'></div>",
  _bg = "<div class='layout-popup-background'><div class='poplogin-bg-layer1'></div><div class='poplogin-bg-layer2'></div><div class='poplogin-bg-layer3'></div></div>";
  this.popupWrap = $('<div />');
  this.popupWrap.html(_wrap + _bg);
}
POPUP_FEATURE.prototype = {
  popup: function(content) {
    this.popupWrap.find('.layout-popup-wrap').html(content);
  },
  popupOpen: function() {
    var event = jQuery.Event("popupOpen");
    // event.name = "wdjfioejfiejifje"
    $('body')
    .append(this.popupWrap.html())
    .trigger(event)
  },
  popupClose: function() {
    var event = jQuery.Event("popupClose");
    $('body')
    .find('.layout-popup-background, .layout-popup-wrap')
    .fadeOut(300, function() { $(this).remove(); })
      // .remove()
      .end()
      .trigger(event)
    }
  }
  var POPMsg = new POPUP_FEATURE();
  
  function popAlert(mytext){
    var mystring = mytext.toString();
    $("body").find(".layout-popupalert-wrap,.layout-popupalert-background").remove();
    $("body").append('<div class="layout-popupalert-wrap"><div class="layout-popupalert-box"><!--i class="btn-popclose"></i--><div class="layout-popupalert-content">'+mystring+'</div></div></div><div class="layout-popupalert-background"></div>');
  }


  //Popup For Login Actions
  // function popLogin(){
  //   POPMsg.popupClose();  
  //   var popupContent = $("#poplogin").html();
  //   POPMsg.popup(popupContent);
  //   POPMsg.popupOpen();
  //   $("body").find(".layout-popup-background").addClass("style-poplogin");
  //   setTimeout( '$("body").find(".layout-popup-background,.layout-popup-wrap").addClass("is-on");' , 100);
  // };
  // $("body").on('click', '.open-poplogin', function() {
  //   popLogin();
  // });
 
//EX: pagename.html#a=xxx&b=yyy
function hashToObject() {
  var pairs = window.location.hash.substring(1).split("&"),
    obj = {},
    pair,
    i;

  for ( i in pairs ) {
    if ( pairs[i] === "" ) continue;

    pair = pairs[i].split("=");
    obj[ decodeURIComponent( pair[0] ) ] = decodeURIComponent( pair[1] );
  }

  return obj;
}
var hashObj = hashToObject();

//on Document Ready
(function() {

  $(window).scroll(function () {
    var scrollVal = $(this).scrollTop();
    // console.log("scroll:"+scrollVal);
    $(".layout-view-banner .inner").css( "margin-top" , scrollVal*(0.75)+"px" );
  });

  // popAlert("MY ALERT!");

  //Header
  $(".btn-hamburger").on('click', function(event) {
    // event.preventDefault();
    /* Act on the event */
    $("body").toggleClass('is-openmenu');
  });
  $("body").on('click', '#layout-view,footer', function(event) {
    // event.preventDefault();
    /* Act on the event */
    if( $("body").hasClass('is-openmenu') ){
      $("body").removeClass('is-openmenu');
    }
  });


  $("body").on('keypress', 'input', function(event) {
  // event.preventDefault();
  /* Act on the event */
  // console.log(event.which);
  if ( event.which == 13){
    //PRESS ENTER

    // console.log(this);
    // if( $(this).hasClass('poplogin-input-id') ){
    //   $(this).closest(".form-block").next(".form-block").find(".poplogin-input-pw").focus();
    // }

  }
});




  $("body").on('click', '.layout-popup-background, .popup-shield-shadow, .btn-popclose, .do-popclose', function() {
    POPMsg.popupClose();
  });

  //popAlert
  $("body").on('click', '.layout-popupalert-background,.layout-popupalert-wrap .btn-popclose, .do-popclose', function() {
    $("body").find(".layout-popupalert-wrap,.layout-popupalert-background").remove();
  });

  //Accordion
  $("body").find(".level2-item a:first-child").on('click', function() {
    $(this).closest('.level2-item').toggleClass("is-open");
  });

  // 執行 FastClick
  FastClick.attach(document.body);

})();